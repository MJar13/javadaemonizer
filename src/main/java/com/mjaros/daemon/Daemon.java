package com.mjaros.daemon;

/**
 * The Interface of Daemon (system Service). Should be compatible with Apache Commons Daemon
 *
 * @see http://commons.apache.org/proper/commons-daemon/
 */
public interface Daemon {

    /**
     * Invoked as your application is initializing. This is where you should set up any initial state for your application.
     * Apart from set up and allocation of native resources, this method must not start the actual operation of the Daemon
     *
     * @param args the args
     * @throws Exception Any exception preventing a successful initialization.
     */
    void init(final String[] args) throws Exception;

    /**
     * Invoked after {@link #init(String[])}. This is where you should begin performing work. Implementors of this method are
     * free to start any number of threads, but need to return control after having done that to enable invocation of
     * {@link #stop()}.
     *
     * @throws Exception Any exception preventing from successfully starting Daemon.
     */
    void start() throws Exception;

    /**
     * Invoked when a daemon has been instructed to stop. This is where you should halt whatever processing you began in
     * {@link #start()}.
     *
     * @throws Exception Any exception that occured while stoping the Daemon
     */
    void stop() throws Exception;

    /**
     * Invoked after {@link #stop()}, but before the JVM process exits. In a traditional Java program, this is where you
     * would free any resources you had acquired. The Daemon can not be restarted after this method has been called without a
     * new call to the {@link #init(String[])} method.
     */
    void destroy();
}
