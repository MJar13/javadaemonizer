package com.mjaros.daemon;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDaemon implements Daemon {

    private static final Logger log = LoggerFactory.getLogger(AbstractDaemon.class);
    private static final String PID_FILE_PROPERTY = "daemon.pidFile";
    private transient Thread daemon;
    private transient boolean working;

    public void daemonize(final String[] args) throws Exception {
        initDeamon(args);
        startDeamon();
        while (isWorking()) {
        }
        destroyDeamon();
    }

    private void initDeamon(final String[] args) throws Exception {
        daemon = findDaemonThread();
        addShutdownHook();
        setPidFileProperties();
        init(args);
        detachFromConsole();
    }

    private Thread findDaemonThread() {
        return Thread.currentThread();
    }

    private void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                try {
                    stopDeamon();
                } catch (final Exception e) {
                    log.error("Error ocured while stopping the dameon", e);
                }
            }
        });
    }

    private void setPidFileProperties() {
        final File pidFile = getPidFile();
        if (pidFile != null) {
            pidFile.deleteOnExit();
        }
    }

    private File getPidFile() {
        final String pidFilePath = getPidFilePath();
        if (pidFilePath == null) {
            return null;
        } else {
            return new File(pidFilePath);
        }
    }

    private String getPidFilePath() {
        return System.getProperty(PID_FILE_PROPERTY);
    }

    private void detachFromConsole() {
        System.out.close();
        System.err.close();
    }

    private void startDeamon() throws Exception {
        working = true;
        start();
    }

    private boolean isWorking() {
        return working;
    }

    private void stopDeamon() throws Exception {
        try {
            stop();
            working = false;
            daemon.join();
        } catch (final InterruptedException e) {
            log.error("Error waiting for the main thread to join", e);
        }
    }

    private void destroyDeamon() {
        destroy();
    }
}
