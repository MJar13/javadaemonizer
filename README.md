# README #

## Java Daemonizer ##

### Goal of the project ###

Purpose of this project is to provide easy to apply template for changing
a Java application into daemon/service.

### Project inspiration ###

* The source code is based on tutorial available on
  http://barelyenough.org/blog/2005/03/java-daemon/
* Provided daemon interface should also be compatible with
  [Apache Commons Daemon](http://commons.apache.org/proper/commons-daemon/).

### License ###

New BSD License. For full license information read LICENSE file.
